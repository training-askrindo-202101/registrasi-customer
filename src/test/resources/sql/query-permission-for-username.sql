select u.username, p.value as authority
from s_permission p
inner join s_role_permission rp on rp.id_permission = p.id
inner join s_role r on rp.id_role = r.id
inner join s_user u on u.id_role = r.id
where u.username = 'user001@yopmail.com';