insert into customer (id, nama, email, tanggal_lahir)
values ('c001', 'Customer 001', 'c001@yopmail.com', '2001-12-31');

insert into alamat_pengiriman (id, id_customer, nama_alamat, nama_penerima, alamat)
values ('a001', 'c001', 'Rumah', 'Customer 001', 'Bogor');

insert into alamat_pengiriman (id, id_customer, nama_alamat, nama_penerima, alamat)
values ('a002', 'c001', 'Kantor', 'Customer 001 (IT Dept)', 'Jakarta');

insert into customer (id, nama, email, tanggal_lahir)
values ('c002', 'Customer 002', 'c002@yopmail.com', '2010-12-31');
