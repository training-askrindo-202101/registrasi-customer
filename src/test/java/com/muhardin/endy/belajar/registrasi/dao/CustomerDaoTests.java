package com.muhardin.endy.belajar.registrasi.dao;

import java.time.LocalDate;
import java.util.List;

import com.muhardin.endy.belajar.registrasi.dto.CustomerInfo;
import com.muhardin.endy.belajar.registrasi.entity.Customer;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql(scripts = {"/sql/clear-data.sql", "/sql/sample-customer.sql"})
public class CustomerDaoTests {

    @Autowired private CustomerDao customerDao;

    //@Test
    public void testAmbilSemuaCustomer(){
        Iterable<Customer> hasil = customerDao.findAll();
        for(Customer c : hasil){
            System.out.println("Nama : "+c.getNama());
            System.out.println("Email : "+c.getEmail());
            System.out.println("Tanggal Lahir : "+c.getTanggalLahir());
        }
    }

    //@Test
    public void testCariByEmail(){
        String email = "yopmail.com";
        List<Customer> hasil = customerDao.findByEmail(email);
        tampilkanData(hasil);

        List<Customer> hasil2 = customerDao.findByEmailContaining(email);
        tampilkanData(hasil2);
    }

    //@Test
    public void testCariTanggalLahir(){
        LocalDate tanggalAwal = LocalDate.of(2000, 1, 1);
        LocalDate tanggalAkhir = LocalDate.of(2002, 1, 1);

        List<Customer> hasil = customerDao.findByTanggalLahirBetween(tanggalAwal, tanggalAkhir);
        tampilkanData(hasil);
    }

    //@Test
    public void cariByNamaAtauEmail(){
        String search = "002";
        List<Customer> hasil = customerDao.findByEmailContainingOrNamaContaining(search, search);
        tampilkanData(hasil);
    }

    @Test
    public void cariCustomerInfo(){
        CustomerInfo hasil = customerDao.findCustomerInfoByEmail("c002@yopmail.com");
        System.out.println("Nama : "+hasil.getNama());
        System.out.println("Email : "+hasil.getEmail());
        System.out.println("Info : "+hasil.getInfo());
    }

    //@Test
    public void cariCustomerExample(){
        Customer c = new Customer();
        c.setEmail("c001@yopmail.com");
        c.setNama("Customer 001");
        Example<Customer> exCust = Example.of(c);

        Iterable<Customer> hasil = customerDao.findAll(exCust);
        tampilkanData(hasil);
    }

    private void tampilkanData(List<Customer> hasil){
        for(Customer c : hasil){
            System.out.println("Nama : "+c.getNama());
            System.out.println("Email : "+c.getEmail());
            System.out.println("Tanggal Lahir : "+c.getTanggalLahir());
        }
    }

    private void tampilkanData(Iterable<Customer> hasil){
        for(Customer c : hasil){
            System.out.println("Nama : "+c.getNama());
            System.out.println("Email : "+c.getEmail());
            System.out.println("Tanggal Lahir : "+c.getTanggalLahir());
        }
    }
}
