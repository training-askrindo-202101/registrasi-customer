package com.muhardin.endy.belajar.registrasi.service;

import java.io.IOException;
import java.time.LocalDate;

import com.muhardin.endy.belajar.registrasi.entity.Customer;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql(scripts = "/sql/clear-data.sql")
public class RegistrasiServiceTests {
    @Autowired private RegistrasiService registrasiService;

    @Test
    public void testRegistrasi() throws IOException {
        Customer c = new Customer();
        c.setEmail("c99@yopmail.com");
        c.setNama("Customer 99");
        c.setTanggalLahir(LocalDate.now());

        registrasiService.registrasi(c);
    }
}
