package com.muhardin.endy.belajar.registrasi.dao;

import java.util.List;

import com.muhardin.endy.belajar.registrasi.entity.AlamatPengiriman;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql(scripts = {"/sql/clear-data.sql", "/sql/sample-customer.sql"})
public class AlamatPengirimanDaoTests {
    @Autowired private AlamatPengirimanDao alamatPengirimanDao;

    @Test
    public void testCariByNamaDanEmailCustomer(){
        String pencarian = "001";
        List<AlamatPengiriman> hasil = 
        alamatPengirimanDao.findByCustomerNamaContainingOrCustomerEmailContaining(pencarian, pencarian);
        tampilkanData(hasil);
    }

    private void tampilkanData(List<AlamatPengiriman> hasil){
        for(AlamatPengiriman a : hasil){
            System.out.println("Keterangan : "+a.getNamaAlamat());
            System.out.println("Nama Penerima : "+a.getNamaPenerima());
            System.out.println("Alamat : "+a.getAlamat());
            System.out.println("Email : "+a.getCustomer().getEmail());
        }
    }
}
