package com.muhardin.endy.belajar.registrasi;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class EncodePasswordTests {

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Test
    public void testEncodePassword() {
        String password = "user002";
        String hashed = passwordEncoder.encode(password);
        System.out.println("Password : "+password);
        System.out.println("BCrypt : "+hashed);
    }

    @Test
    public void testVerifikasiPassword() {
        String hashed = "$2a$10$t3nyE4aPJiYNVWEgRcdCmuMHt2OiN7W9jc/lKx1O6nM.sVPkpvb4u";
        String password = "user001";

        Boolean cocok = passwordEncoder.matches(password, hashed);
        System.out.println("Hasil verifikasi : "+cocok);
    }
}
