insert into s_role(id, label, value) values
('r001', 'Super User', 'SUPERUSER');

insert into s_role(id, label, value) values
('r002', 'Staff', 'STAFF');

insert into s_permission(id, label, value) values
('p001', 'View Customer', 'VIEW_CUSTOMER');

insert into s_permission(id, label, value) values
('p002', 'Edit Customer', 'EDIT_CUSTOMER');

-- super user boleh view dan edit customer
insert into s_role_permission (id_role, id_permission) values ('r001', 'p001');
insert into s_role_permission (id_role, id_permission) values ('r001', 'p002');

-- staff cuma boleh view customer
insert into s_role_permission (id_role, id_permission) values ('r002', 'p001');

insert into s_user(id, username, fullname, active, id_role) values
('u001', 'user001@yopmail.com', 'User 001', true, 'r001');

insert into s_user(id, username, fullname,active, id_role) values
('u002', 'user002@yopmail.com', 'User 002', true, 'r002');

insert into s_user_password (id_user, hashed_password) values
('u001', 'user001');

insert into s_user_password (id_user, hashed_password) values
('u002', 'user002');