create table s_role (
    id varchar (36),
    label varchar (50) not null,
    value varchar (50) not null,
    primary key (id),
    unique (label),
    unique (value)
);
create table s_permission (
    id varchar (36),
    label varchar (50) not null,
    value varchar (50) not null,
    primary key (id),
    unique (label),
    unique (value)
);
create table s_role_permission (
    id_role varchar (36),
    id_permission varchar (36),
    primary key (id_role, id_permission),
    foreign key (id_role) references s_role,
    foreign key (id_permission) references s_permission
);
create table s_user (
    id varchar (36),
    username varchar (100) not null,
    fullname varchar (255) not null,
    active boolean not null default true,
    id_role varchar (36) not null,
    primary key (id),
    unique (username),
    foreign key (id_role) references s_role
);
create table s_user_password (
    id_user varchar (36),
    hashed_password varchar (255) not null,
    primary key (id_user),
    foreign key (id_user) references s_user
);
