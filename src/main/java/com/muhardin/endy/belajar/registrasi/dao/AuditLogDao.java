package com.muhardin.endy.belajar.registrasi.dao;

import com.muhardin.endy.belajar.registrasi.entity.AuditLog;

import org.springframework.data.repository.CrudRepository;

public interface AuditLogDao extends CrudRepository<AuditLog, String> {
    
}
