package com.muhardin.endy.belajar.registrasi.service;

import java.io.IOException;

import com.muhardin.endy.belajar.registrasi.dao.CustomerDao;
import com.muhardin.endy.belajar.registrasi.entity.Customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RegistrasiService {

    @Autowired private CustomerDao customerDao;
    @Autowired private AuditLogService auditLogService;

    @Transactional(rollbackFor = IOException.class)
    public void registrasi(Customer customer) throws IOException {
        auditLogService.log("Pendaftaran customer baru : "+customer.getNama());
        customerDao.save(customer);

        // pemanggilan ini tidak melalui proxy, 
        // karena langsung mengarah ke target object, yaitu object this
        aktivasi(customer); 

        //throw new IllegalStateException("Simulasi error");
        throw new IOException("terjadi error ");
    }

    public void aktivasi(Customer customer){
        
    }
}
