package com.muhardin.endy.belajar.registrasi.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Entity 
//@Table(name = "m_customer")
public class Customer {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(length = 36)
    private String id;

    @NotNull @NotEmpty @Size(min = 2, max = 100)
    @Column(length = 100, nullable = false)
    private String nama;

    @Email @NotNull @NotEmpty @Size(min = 10, max = 100)
    @Column(length = 100, nullable = false, unique = true)
    private String email;

    @NotNull @Past
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(nullable = false)
    private LocalDate tanggalLahir;
}
