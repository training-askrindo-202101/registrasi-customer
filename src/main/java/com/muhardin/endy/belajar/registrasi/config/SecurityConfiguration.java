package com.muhardin.endy.belajar.registrasi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.sql.DataSource;

@Configuration @EnableMethodSecurity
public class SecurityConfiguration {

    private static final String SQL_LOGIN
            = "select u.username, up.hashed_password as password, u.active as enabled " +
            "from s_user u " +
            "inner join s_user_password up " +
            "on u.id = up.id_user " +
            "where u.username = ?";

    private static final String SQL_PERMISSION
            = "select u.username, p.value as authority " +
            "from s_permission p " +
            "inner join s_role_permission rp on rp.id_permission = p.id " +
            "inner join s_role r on rp.id_role = r.id " +
            "inner join s_user u on u.id_role = r.id " +
            "where u.username = ?";

    @Bean
    UserDetailsManager users(DataSource dataSource) {
        JdbcUserDetailsManager userManager = new JdbcUserDetailsManager(dataSource);
        userManager.setUsersByUsernameQuery(SQL_LOGIN);
        userManager.setAuthoritiesByUsernameQuery(SQL_PERMISSION);
        return userManager;
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        //return NoOpPasswordEncoder.getInstance(); // jangan pakai plain text, tidak secure
        return new BCryptPasswordEncoder();
    }
}
