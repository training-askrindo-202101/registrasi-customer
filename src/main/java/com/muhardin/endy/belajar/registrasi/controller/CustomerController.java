package com.muhardin.endy.belajar.registrasi.controller;

import com.muhardin.endy.belajar.registrasi.dao.CustomerDao;
import com.muhardin.endy.belajar.registrasi.dto.CustomerInfo;
import com.muhardin.endy.belajar.registrasi.entity.Customer;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@Controller @Slf4j
public class CustomerController {

    private static final Integer MIN_UMUR_TAHUN = 17;
    @Autowired private CustomerDao customerDao;

    @Autowired private DataSource dataSource;

    @Value("${upload.folder}")
    private String lokasiUpload;

    @Autowired @Qualifier("reportCustomerArrayList")
    private JasperReport reportCustomerArrayList;

    @Autowired @Qualifier("reportCustomerSql")
    private JasperReport reportCustomerSql;

    @Autowired
    private JRSwapFileVirtualizer jasperVirtualizer;

    @GetMapping("/customer/report/xlsx")
    public void customerReportXls(HttpServletResponse response) {
        try {
            Workbook workbook = new XSSFWorkbook();

            Sheet sheet = workbook.createSheet("Persons");
            sheet.setColumnWidth(0, 6000);
            sheet.setColumnWidth(1, 4000);

            Row header = sheet.createRow(0);

            CellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
            headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            XSSFFont font = ((XSSFWorkbook) workbook).createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 16);
            font.setBold(true);
            headerStyle.setFont(font);

            Cell headerCell = header.createCell(0);
            headerCell.setCellValue("Nama");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(1);
            headerCell.setCellValue("Email");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(2);
            headerCell.setCellValue("Tanggal Lahir");
            headerCell.setCellStyle(headerStyle);

            CellStyle style = workbook.createCellStyle();
            style.setWrapText(true);

            Iterable<Customer> dataCustomer = customerDao.findAll();
            AtomicReference<Integer> baris = new AtomicReference<>(2);
            dataCustomer.forEach(c -> {
                Row row = sheet.createRow(baris.getAndSet(baris.get() + 1));
                Cell cell = row.createCell(0);
                cell.setCellValue(c.getNama());
                cell.setCellStyle(style);

                cell = row.createCell(1);
                cell.setCellValue(c.getEmail());
                cell.setCellStyle(style);

                cell = row.createCell(2);
                cell.setCellValue(c.getTanggalLahir().format(DateTimeFormatter.BASIC_ISO_DATE));
                cell.setCellStyle(style);
            });

            // render hasilnya
            // Content-Type = application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
            // Content-Disposition : inline (langsung tampil) atau attachment (download file)
            // Untuk attachment, kita sebutkan nama filenya
            String namafile = "customer-report-"
                    + DateTimeFormatter.BASIC_ISO_DATE.format(LocalDate.now())
                    + ".xlsx";
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition", "attachment;filename=" + namafile);

            workbook.write(response.getOutputStream());
            workbook.close();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @GetMapping("/customer/report/large")
    public void customerReportLarge(HttpServletResponse response) {
        try {

            // data untuk parameter dalam Jasper
            Map<String, Object> parameterJasper = new HashMap<>();
            parameterJasper.put("waktuCetak", LocalDateTime.now());

            // kasi connection saja ke Jasper Report, nanti dia yang akan connect ke database
            Connection dbConnection = dataSource.getConnection();

            // virtualizer untuk temporary file
            parameterJasper.put(JRParameter.REPORT_VIRTUALIZER, jasperVirtualizer);

            // merge data dengan template
            JasperPrint jrPrint = JasperFillManager.fillReport(reportCustomerSql,
                    parameterJasper, dbConnection);

            renderPdf(response, jrPrint);
            jasperVirtualizer.cleanup();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @GetMapping("/customer/report/small")
    public void customerReportSmall(HttpServletResponse response) {

        try {
            // data untuk parameter dalam Jasper
            Map<String, Object> parameterJasper = new HashMap<>();
            parameterJasper.put("waktuCetak", LocalDateTime.now());

            // ambil data dari database -> untuk band detail
            Iterable<Customer> customers = customerDao.findAll();
            List<Customer> customerList = new ArrayList<>();
            customers.forEach(customerList::add);
            JRBeanCollectionDataSource dataSource
                    = new JRBeanCollectionDataSource(customerList);

            // merge data dengan template
            JasperPrint jrPrint = JasperFillManager.fillReport(reportCustomerArrayList,
                    parameterJasper, dataSource);
            renderPdf(response, jrPrint);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @GetMapping("/api/customer/") 
    @ResponseBody
    public Page<Customer> dataCustomer(Pageable page){
        // versi tanpa paging
        // return customerDao.findAll();

        // versi dengan paging
        return customerDao.findAll(page);
    }

    @GetMapping("/api/customer/email/") 
    @ResponseBody
    public CustomerInfo dataCustomerInfo(@RequestParam String email){
        return customerDao.findCustomerInfoByEmail(email);
    }

    @GetMapping("/customer/list")
    @PreAuthorize("hasAuthority('VIEW_CUSTOMER')")
    public void dataCustomerHtml(Model model, Pageable page){
            model.addAttribute("customerData", customerDao.findAll(page));
    }

    @GetMapping("/customer/form")
    @PreAuthorize("hasAuthority('EDIT_CUSTOMER')")
    public ModelMap tampilkanFormEditCustomer(){
        ModelMap mm = new ModelMap();
        mm.addAttribute("customer", new Customer());
        return mm;
    }

    @PostMapping("/customer/form")
    public String prosesFormEdit(
        HttpServletRequest request,
        @RequestParam String cabang,
        @RequestParam("foto") MultipartFile foto,
        @ModelAttribute @Valid Customer customer, 
        BindingResult errors, SessionStatus sessionStatus, 
        RedirectAttributes redirectAttributes){

        log.debug("Cabang pendaftar (pakai @RequestParam) : {}", cabang);
        log.debug("Cabang pendaftar (pakai HttpServletRequest) : {}" ,request.getParameter("cabang"));

        LocalDate tanggalLahir17TahunLalu = 
            LocalDate.now().minusYears(MIN_UMUR_TAHUN);

        // validasi umur 17 tahun
        if(customer.getTanggalLahir().isAfter(tanggalLahir17TahunLalu)) {
            FieldError errorKurangUmur = new FieldError(
                "customer", "tanggalLahir", customer.getTanggalLahir(), 
                false, new String[]{"Age17"}, null, "belum cukup umur");
            errors.addError(errorKurangUmur);
        }

        if (errors.hasErrors()) {
            return "/customer/form";
        }

        customerDao.save(customer);

        prosesUploadFoto(foto, customer);

        sessionStatus.setComplete();

        redirectAttributes
            .addFlashAttribute("sukses", "Data customer "+customer.getNama()+" berhasil disimpan");

        return "redirect:list";
    }

    private void renderPdf(HttpServletResponse response, JasperPrint jrPrint) throws JRException, IOException {
        // render hasilnya
        // Content-Type = application/pdf
        // Content-Disposition : inline (langsung tampil) atau attachment (download file)
        // Untuk attachment, kita sebutkan nama filenya
        String namafile = "customer-report-"
                + DateTimeFormatter.BASIC_ISO_DATE.format(LocalDate.now())
                + ".pdf";
        response.setContentType("application/pdf");
        //response.setHeader("Content-Disposition", "attachment;filename="+namafile);
        response.setHeader("Content-Disposition", "inline");
        JasperExportManager.exportReportToPdfStream(jrPrint, response.getOutputStream());
    }

    private void prosesUploadFoto(MultipartFile foto, Customer customer) {
        try {
            log.debug("Nama file yang diupload : {}", foto.getOriginalFilename());
            log.debug("Ukuran file : {}", foto.getSize());
            log.debug("Jenis file : {}", foto.getContentType());

            File folderUpload = new File(lokasiUpload);
            folderUpload.mkdirs();


            String namafileTujuan = customer.getId() + extensionFile(foto);
            File fileTujuan = new File(folderUpload.getAbsolutePath() + File.separator + namafileTujuan);
            foto.transferTo(fileTujuan);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    private String extensionFile(MultipartFile foto){
        String namafile = foto.getOriginalFilename();
        String extensionAsli = foto.getOriginalFilename().substring(namafile.lastIndexOf("."), namafile.length());
        log.debug("Extension asli = {}", extensionAsli);
        MediaType mediaType = MediaType.parseMediaType(foto.getContentType());
        if(MediaType.IMAGE_JPEG.isCompatibleWith(mediaType)) {
            return ".jpg";
        }
        if (MediaType.IMAGE_PNG.isCompatibleWith(mediaType)) {
            return ".png";
        }
        return extensionAsli;
    }
}
