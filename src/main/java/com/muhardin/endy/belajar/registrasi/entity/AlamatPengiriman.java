package com.muhardin.endy.belajar.registrasi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Entity
public class AlamatPengiriman {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(length = 36)
    private String id;

    @ManyToOne @JoinColumn(name = "id_customer", nullable = false)
    private Customer customer;

    @Column(length = 100, nullable = false)
    private String namaAlamat;

    @Column(length = 100)
    private String namaPenerima;

    @Column(nullable = false)
    private String alamat;
}
