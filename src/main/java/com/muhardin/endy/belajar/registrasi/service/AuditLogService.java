package com.muhardin.endy.belajar.registrasi.service;

import com.muhardin.endy.belajar.registrasi.dao.AuditLogDao;
import com.muhardin.endy.belajar.registrasi.entity.AuditLog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuditLogService {

    @Autowired private AuditLogDao auditLogDao;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void log(String message){
        AuditLog log = new AuditLog();
        log.setMessage(message);
        auditLogDao.save(log);
    }
}
