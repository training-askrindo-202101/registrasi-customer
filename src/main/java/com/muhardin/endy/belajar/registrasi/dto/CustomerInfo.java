package com.muhardin.endy.belajar.registrasi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class CustomerInfo {
    private String nama;
    private String email;

    public String getInfo(){
        return nama + "<"+email+">";
    }
}
