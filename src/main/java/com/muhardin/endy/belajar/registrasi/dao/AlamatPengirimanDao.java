package com.muhardin.endy.belajar.registrasi.dao;

import java.util.List;

import com.muhardin.endy.belajar.registrasi.entity.AlamatPengiriman;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface AlamatPengirimanDao extends PagingAndSortingRepository<AlamatPengiriman, String> {
    List<AlamatPengiriman> findByCustomerNamaContainingOrCustomerEmailContaining(String nama, String email);
}
