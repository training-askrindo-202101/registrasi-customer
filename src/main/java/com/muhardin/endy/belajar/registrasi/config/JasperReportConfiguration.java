package com.muhardin.endy.belajar.registrasi.config;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.InputStream;

@Configuration @Slf4j
public class JasperReportConfiguration {

    @Value("${jasper.virtualizer.folder}")
    private String folderTemporaryJasperReport;

    @Value("classpath:/jasperreports/customer_list.jrxml")
    private Resource templateReportCustomerArrayList;

    @Value("classpath:/jasperreports/customer_list_sql.jrxml")
    private Resource templateReportCustomerSql;

    @PostConstruct
    public void inisialisasiFolderVirtualizer() {
        new File(folderTemporaryJasperReport).mkdirs();
    }

    @Bean
    public JRSwapFileVirtualizer swapFileVirtualizer() {
        return new JRSwapFileVirtualizer(20,
                new JRSwapFile(folderTemporaryJasperReport, 1024, 100),
                true);
    }

    @Bean
    public JasperReport reportCustomerArrayList() {
        log.debug("Compile template report arraylist");
        try {
            return compileTemplate(templateReportCustomerArrayList.getInputStream());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    @Bean
    public JasperReport reportCustomerSql() {
        log.debug("Compile template report sql");
        try {
            return compileTemplate(templateReportCustomerSql.getInputStream());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    private JasperReport compileTemplate(InputStream template) throws JRException {
            return JasperCompileManager.compileReport(template);
    }
}
