package com.muhardin.endy.belajar.registrasi.dao;

import java.time.LocalDate;
import java.util.List;

import com.muhardin.endy.belajar.registrasi.dto.CustomerInfo;
import com.muhardin.endy.belajar.registrasi.entity.Customer;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String>, QueryByExampleExecutor<Customer> {
    List<Customer> findByEmail(String email);
    List<Customer> findByEmailContaining(String email);
    List<Customer> findByTanggalLahirBetween(LocalDate mulai, LocalDate sampai);
    List<Customer> findByEmailContainingOrNamaContaining(String email, String nama);

    @Query("select new com.muhardin.endy.belajar.registrasi.dto.CustomerInfo(c.nama, c.email) from Customer c where c.email = :customerEmail")
    CustomerInfo findCustomerInfoByEmail(@Param("customerEmail") String x);
}
