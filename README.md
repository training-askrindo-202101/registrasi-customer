# Aplikasi Registrasi Customer #

Menjalankan aplikasi

```
mvn clean spring-boot:run
```

Kemudian browse ke [http://localhost:8080](http://localhost:8080)

## Menjalankan Database PostgreSQL ##

Database dijalankan menggunakan docker, dengan perintah

Di Linux

```
docker run --rm --name customerdb -e POSTGRES_USER=customer -e POSTGRES_PASSWORD=customer123 -e POSTGRES_DB=customerdb -v "$PWD/db-customer:/var/lib/postgresql/data" -p 5432:5432 postgres:14
```

Di Windows

```
docker run --rm --name customerdb -e POSTGRES_USER=customer -e POSTGRES_PASSWORD=customer123 -e POSTGRES_DB=customerdb -v "%cd%/db-customer:/var/lib/postgresql/data" -p 5432:5432 postgres:14
```